# Roxyp.dev website

Source code of [roxyp.dev](https://roxyp.dev) website.

Built with [Eleventy](https://www.11ty.dev/).

# Build

```
npm i
npm run build
```
