---
title: Roxyp
layout: base.liquid
---

Roxyp is under blazingly slow development.

What I want it to be:
- A mock server written in Rust
- A stateless reverse proxy with http caching, circuit breaking, retries and rate limiting
- Configurable in Typescript, preferably at build time
- My side project for the next few years
