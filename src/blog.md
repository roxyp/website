---
title: Blog
layout: base.liquid
---

<ul>
{% for post in collections.blog %}
  <li><a href="{{ post.url | url }}">{{ post.data.created }} {{ post.data.title }}</a></li>
{% endfor %}
</ul>