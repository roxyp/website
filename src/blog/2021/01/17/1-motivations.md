---
title: Motivations
layout: blog.liquid
author: Guillaume
created: "2021-01-17"
place: Santiago, Chile
tags:
- blog
- roxyp
---

## Learning

Rust picked my interest a few years ago. I know it's good and I want to learn it. Unfortunately I wasn't able to find a job that would let me use it, so I decided to learn it building this side project.

Hopefully this would be more fun than the usual Spring Boot APIs I'm working on all day long.

## Mocking

At work I regularly have to interface with APIs (RESTish, JSON) that are not implemented yet.
So I use a mock server.

Mock servers are also useful for automated integration tests, where you want to test your API without calling the real service, which may not be accesible in your test environment for example.

Mocking should be similar to serving static files, I guess, except you want to control the status code that the endpoint returns, and also simulate slow or buggy backends.

## Proxying

I love Nginx and have used it for serving static files and as a reverse proxy to a backend API with or without http caching.
There are a few things that I want Roxyp to do that Nginx does not (as far as I know):
- Validable configuration: This is why I think Typescript is a good fit (I liked how [K6](https://k6.io/), a load testing tool, use Javascript as a configuration language, despite being programmed in Go), also I like Typescript.
- Stateless caching: I once used Nginx as an http cache between two microservices, in a Kubernetes setup, and it worked great, except that it was not scalable because Nginx use an in-memory structure to map keys (urls) with files. So I ended up configuring it to use only one pod.
- Circuit breaking, retries, rate limiting: Those are features that I wished once Nginx had. [Envoy](https://www.envoyproxy.io/) and maybe HAProxy have them so these could be good sources of inspiration.

## I've been asked to make a POC

I had to make a POC (Proof Of Concept) of a Java library for circuit breaking: [Resilience4j](https://resilience4j.readme.io/) which is a great library. But how do you test such a library? Well, you need a mock server that can simulate errors and slowness. Hence the mocking part of Roxyp.

In the process of doing this POC I also discovered and used [K6](https://k6.io/), [InfluxDB](https://www.influxdata.com/products/influxdb/) and [Grafana](https://grafana.com/grafana/), I used this stack to collect the results of calling two versions of the same API, one with [Resilience4j](https://resilience4j.readme.io/) enabled versus another without it.

Then I find out why I had been asked to do this POC, I will be in charge of a microservice using the ACL (Anti Corruption Layer) pattern, to proxy all requests to a third party service.

So I asked myself: Why don't use an established proxy for that? Instead of implementing it.. badly. Nginx has no circuit breaking feature (in its Open Source version at least), HAProxy or [Envoy](https://www.envoyproxy.io/) would probably fit the bill though, I'll have to check.

[Resilience4j](https://resilience4j.readme.io/) also has a rate limiting capability, but it doesn't make sense to me in a Kubernetes environment, because the library would rate limit at the pod level and this seems useless because you can't know in advance the pods number, except if you disable horizontal pod autoscaling, but I don't want to.

I still have to deliver the POC results so this is a story without an end, yet.

## Having fun

I like programming, I like DevOps, I like designing software architecture.
But at my current company, there is a development team, an architecture team, a devops team, a release team, a middleware team and so on.
I've been labeled as a backend developer so I can't do architecture, I can't do DevOps, I can't release my work at production, I can't even push a git branch without filling a form if you can believe it.
I'm a chain worker in a Spring Boot factory, which I enjoy, but is a bit boring.

So let's have fun with Roxyp, a mocking, reverse-proxying, http-caching, circuit-breaking server built in Rust!

## This website

So I've got this idea, liked it, came up with a name so I bought roxyp.dev, and quickly setup this website with [GitLab](https://gitlab.com) and [Eleventy](https://www.11ty.dev/), a really nice static site generator.

I'm not a frontend expert so this will be pretty spartian.

### Blogging

So now I have a web site, and a side project, so why not blog a little? In fact I accumulate frustration at work and my home life is complicated lately because of some f***ing batvirus, so instead of complaining (I'm french it's my thing) to my colleagues and loved ones I can use this blog as a say-what-i-want place, explaining my journey to Rust and Roxyp, and maybe some other tech related stuff.

#### Becoming rich and famous

As soon as I have something that seems useful, I will open source it, so little hope, but I have the dream to be noticed for this work some day and hopefully employed to do what I like in a big money corp or startup.
