FROM node:lts-alpine AS builder
COPY . /build
WORKDIR /build
RUN ls -la && npm ci && npm run build

FROM nginx:alpine
# One day it will be FROM roxyp :)
COPY --from=builder /build/public /usr/share/nginx/html/