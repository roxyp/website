const markdownIt = require("markdown-it");
const hl = require("highlight.js");

module.exports = function (eleventyConfig) {
  eleventyConfig.setTemplateFormats(["md", "css"]);
  eleventyConfig.setLibrary(
    "md",
    markdownIt({
      html: true,
      highlight: (str, lang) => {
        if (lang && hl.getLanguage(lang)) {
          try {
            return hl.highlight(lang, str).value;
          } catch (e) {
            console.warn(e);
          }
        }
        return "";
      },
    })
  );
  return {
    dir: {
      input: "src",
      output: "public",
    },
  };
};
